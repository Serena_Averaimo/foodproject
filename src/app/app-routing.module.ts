import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './core/layout/layout.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      {path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      {path: 'nav', loadChildren: () => import('./features/features.module').then(m => m.FeaturesModule)},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: '**' , redirectTo: 'home'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
