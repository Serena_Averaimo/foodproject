import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { WhyusComponent } from './whyus/whyus.component';
import { MenuComponent } from './menu/menu.component';
import { SpecialsComponent } from './specials/specials.component';
import { EventsComponent } from './events/events.component';
import { BookingComponent } from './booking/booking.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ChefsComponent } from './chefs/chefs.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ContactComponent } from './contact/contact.component';
import {Route, RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../core/layout/layout.module';

const routes: Routes = [
  { path: 'about', component: AboutComponent},
  { path: 'booking', component: BookingComponent},
  { path: 'chefs', component: ChefsComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'events', component: EventsComponent},
  { path: 'gallery', component: GalleryComponent},
  { path: 'specials', component: SpecialsComponent},
  { path: 'menu', component: MenuComponent}
];

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [AboutComponent, MenuComponent, SpecialsComponent, EventsComponent, BookingComponent, GalleryComponent, ChefsComponent, ContactComponent],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule.forChild(routes)
  ]
})
export class FeaturesModule { }
