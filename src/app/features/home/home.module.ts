import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {WhyusComponent} from '../whyus/whyus.component';
import {TestimonialsComponent} from '../testimonials/testimonials.component';
import {LayoutModule} from '../../core/layout/layout.module';
import {RouterModule} from '@angular/router';
import {CarouselComponent} from '../carousel/carousel.component';
import {NgbCarouselModule} from "@ng-bootstrap/ng-bootstrap";



@NgModule({
  declarations: [HomeComponent, WhyusComponent, TestimonialsComponent, CarouselComponent],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule.forChild([{path: '', component: HomeComponent}]),
    NgbCarouselModule
  ]
})
export class HomeModule { }
